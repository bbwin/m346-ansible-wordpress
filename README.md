# AWS Wordpress Stack with Ansible

Start lab in Learner Lab > Modules and access CLI details
![image-20221212031754599](README.assets/image-20221212031754599-1670811485019-1.png)

This playbook requires the AWS credentials to be set as ENV variables, the names must be exactly as noted in `.env.example`. You can copy this file to `.env`

```sh
cp .env.example .env
vim .env
source .env
```

Run the playbook

```sh
ansible-playbook -i localhost, site.yml
```

